module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'poketeam',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project + Bulma CSS Framework' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Global CSS
  */
  css: [
    { src: '~/assets/scss/app.scss', lang: 'scss' }, { src: '~/assets/scss/_bootstrap_override.scss', lang: 'scss' },
  ],
  modules: [["bootstrap-vue/nuxt", { icons: true }], "@nuxtjs/axios", ["nuxt-vuex-localstorage", { localStorage: ['favorite', 'team'] }]],
  plugins: [
    '~/plugins/webFontLoader.client.js'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    babel: {
      compact: true,
    },
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

