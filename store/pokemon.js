export const state = () => ({
    token: '',
    baseURL: 'https://pokeapi.co/api/v2/'
})

export const mutations = {
    login(state, payload) {
        state.token = payload.token;
    }
}

export const actions = {
    login({ commit }, payload) {
        commit('login', payload);
    }
}

