export const state = () => ({
    favorites: []
})

export const mutations = {
    add(state, name) {
        if (state.favorites.indexOf(name) < 0) {
            state.favorites = [...state.favorites, name];
        }
    },
    remove(state, name) {
        let index = state.favorites.indexOf(name)
        if (index > -1) {
            state.favorites.splice(index, 1);
        }
    }
}

export const actions = {
    add({ commit }, name) {
        commit('add', name);
    },
    remove({ commit }, name) {
        commit('remove', name);
    }
}
