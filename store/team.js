export const state = () => ({
    teams: []
})

export const mutations = {
    add(state, payload) {
        let id = String(Math.random(1024));
        state.teams = [...state.teams, {
            id, name: payload.name, members: payload.members
        }];
        console.log(state.teams);
    },
    edit(state, payload) {
        let id = payload.id;
        state.teams.map((item) => {
            if (item.id == id) {
                item.name = payload.name;
                item.members = payload.members;
            }
            return item;
        })
    },
    remove(state, id) {
        let index = -1;
        state.teams.forEach((item, i) => {
            if (item.id == id) {
                index = i;
            }
        });
        if (index > -1) {
            state.teams.splice(index, 1)
        }
    }
}

export const actions = {
    add({ commit }, payload) {
        commit('add', payload);
    },
    edit({ commit }, payload) {
        commit('add', payload);
    },
    remove({ commit }, id) {
        commit('remove', id);
    }
}
