Create a Nuxt project that allows user to:

See a list of 1000 random pokemon
Search pokemon by name or pokemon number (id)
Create teams (user can have more than one team)
Add up to 6 pokemon to a team (each pokemon can only be used once in all teams)
Name, delete, and edit teams
Favourite pokemon

# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
