import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _5aa9925a = () => interopDefault(import('../pages/favorite.vue' /* webpackChunkName: "pages/favorite" */))
const _69d06bde = () => interopDefault(import('../pages/team/index.vue' /* webpackChunkName: "pages/team/index" */))
const _109e7568 = () => interopDefault(import('../pages/team/form.vue' /* webpackChunkName: "pages/team/form" */))
const _42623030 = () => interopDefault(import('../pages/pokemon/_id.vue' /* webpackChunkName: "pages/pokemon/_id" */))
const _d8b86474 = () => interopDefault(import('../pages/team/_id.vue' /* webpackChunkName: "pages/team/_id" */))
const _2aeac004 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/favorite",
    component: _5aa9925a,
    name: "favorite"
  }, {
    path: "/team",
    component: _69d06bde,
    name: "team"
  }, {
    path: "/team/form",
    component: _109e7568,
    name: "team-form"
  }, {
    path: "/pokemon/:id?",
    component: _42623030,
    name: "pokemon-id"
  }, {
    path: "/team/:id",
    component: _d8b86474,
    name: "team-id"
  }, {
    path: "/",
    component: _2aeac004,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
